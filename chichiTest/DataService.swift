//
//  DataService.swift
//  chichiTest
//
//  Created by Arash on 12/18/1395 AP.
//  Copyright © 1395 arashgood. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataService {
    
    static let instance = DataService()

    
    var images = [Image]()
    
//    var numberOfImages = 0


    func downloadImages(callBack: @escaping (_ resp: [Image])  -> Void) {
        
       // DispatchQueue.main.async {
            
            Alamofire.request(MAIN_URL, method: .get).responseJSON { response in
                
                if  response.value != nil {
                    
                    //           print(response.data)
                    
                    let json = JSON(response.result.value!)
                    
                    
                    //            print(json)
                    
                    
                    let cards = json["cards"]
                    
                    
                    
                    for i in 0..<cards.count {
                        
                        let title = cards[i,"title"].stringValue
                        
                        var desc = ""
                        
                        if cards[i,"text"].stringValue.isEmpty {
                            
                            desc = "بدون توضیح"
                            
                        } else {
                            desc = cards[i,"text"].stringValue
                        }
                        
                        var imageStrUrl = ""
                        
                        if cards[i,"image"].stringValue.isEmpty {
                            imageStrUrl = "null"
                        } else {
                            imageStrUrl = cards[i,"image"].stringValue
                        }
                        
                        
                        let img = Image(title: title, desc: desc, StrUrl: imageStrUrl)
                        
                        if img.imgStrUrl != "null" {
                        
                       print("\(i+1) = title is : \(title)\n description is : \(desc) \n imageURL is : \(imageStrUrl)\n\n")
                        
                        self.images.append(img)
                            
                        }
                        
                    }
                    
                    
                    
                    //end of fisrt if
                }
                
                callBack(self.images)
            }
            
          //end of func
        }
        
        //end of Dispatch que
   // }
        
    

    
        
    
    
    



    
    
    
    
}
