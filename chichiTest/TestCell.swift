//
//  TestCell.swift
//  chichiTest
//
//  Created by Arash on 12/12/1395 AP.
//  Copyright © 1395 arashgood. All rights reserved.
//

import UIKit

class TestCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UITextView!
    @IBOutlet weak var cellDesc: UITextView!
    @IBOutlet weak var activityIndicarior: UIActivityIndicatorView! {
        didSet {
            activityIndicarior.startAnimating()
        }
    }
    
    

    
    func configureCell(img: UIImage,title: String, desc: String ) {
    
        cellImage?.image = img
        //make images circle
        cellImage.layer.cornerRadius = (cellImage?.frame.height)!/2
        cellImage.clipsToBounds = true
        
        cellTitle.text = title
        cellDesc.text = desc

        
  
    
    }
    
}
