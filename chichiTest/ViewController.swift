//
//  ViewController.swift
//  chichiTest
//
//  Created by Arash on 12/12/1395 AP.
//  Copyright © 1395 arashgood. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    
    //outlets and vars *********************************************************************************************
    
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var scorllView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var cvActivityIndicator: UIActivityIndicatorView! {
        didSet {
            cvActivityIndicator.startAnimating()
        }
    }
    
    @IBOutlet weak var scActivityIndicator: UIActivityIndicatorView! {
        didSet {
            scActivityIndicator.startAnimating()
        }
    }
    
    
    var images :[Image] = []
    let testImages = [#imageLiteral(resourceName: "eye"),#imageLiteral(resourceName: "lion"),#imageLiteral(resourceName: "something"),#imageLiteral(resourceName: "horse")]
    var tempNumber = 0
    
    
    

    
    //view funcs *********************************************************************************************
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        
//        if isConnectedToNetwork() == true {
//            print("network is ok")
//        } else {
//            print("network problem")
//        }
        
        
            
            //get data
            //DispatchQueue.main.async {
            DataService.instance.downloadImages { (imgs) in
                
                //stop activity indicators
               
                
                self.images = imgs
                self.cvActivityIndicator.stopAnimating()
                self.scActivityIndicator.stopAnimating()
                
                self.tempNumber = self.images.count
                
                
                self.collectionView.reloadData()
                //self.scorllView.refreshControl?.beginRefreshing()
            }
        
        
    //}
       
         
    //end of view will appear
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        scorllView.delegate = self
        
        //end of viewDidLoad
    }
    
    
    
   
    
    override func viewDidLayoutSubviews() {
        
        let cgNumberOfImages = CGFloat(self.images.count)
        let scrollViewWidth = self.scorllView.frame.width
        let scrollViewHeight = self.scorllView.frame.height
        let width = scrollViewHeight * cgNumberOfImages
        
        self.scorllView.contentSize = CGSize(width: width, height: scrollViewHeight)
        
        
        for i in 0  ..< self.images.count {
            let img = UIImageView(frame: CGRect(x: CGFloat(i) * scrollViewWidth, y: 0, width: scrollViewWidth, height: scrollViewHeight))
            
            
            img.image = get_img_from_strUrl(String: self.images[i].imgStrUrl)
            
            scorllView.addSubview(img)
            
            
        }
        
        //page Controller
        configurePageControl()
        pageController.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        

        //end of viewDidLayoutSubviews

    }
    
    
    
    
    
    //CollectionView funcs *******************************************************************************************
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        //return self.images.count
        return self.tempNumber
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CEEEL", for: indexPath) as? TestCell {
           // print("CellforIndex is called")
            
           
                let c = makeCell(cell: cell, img: (images[indexPath.row]))
                
                return c
                
            
            
        } else {
            return UICollectionViewCell()
        }
        
        
      //end of cellForindeX
    }
    
    
    
    // funcs *******************************************************************************************************
        
    
    func makeCell(cell: TestCell,img: Image) -> UICollectionViewCell {
            
            if img.imgStrUrl != "" {
                
                //print("here\n")
                
                //image

                let image = get_img_from_strUrl(String: img.imgStrUrl)
                
                //title
                if img.imgTitle != "" {
                    let imageTitle = img.imgTitle as String
                    
                    //text
                    if img.imgDesc != "" {
                        
                        let imageText = img.imgDesc as String
                        
                       // print("\(image)\n\(imageTitle)\n\(imageText)\n\n")
                        
                        cell.configureCell(img: image, title: imageTitle, desc: imageText)
                        
                        
                        
                        return cell
                        
                        //end of imgTxt if
                    } else {
                        
                        
                        return UICollectionViewCell()
                    }
                    
                    //end of imgTitle if
                } else {
                    return UICollectionViewCell()
                }
                
                
                //end of imgStrUrl if
            } else {
                return UICollectionViewCell()
            }
            
    }

    


    
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        
        //self.pageController.numberOfPages = self.images.count
        self.pageController.numberOfPages = self.tempNumber
        self.pageController.currentPage = 0
        self.pageController.tintColor = UIColor.red
        self.pageController.pageIndicatorTintColor = UIColor.white
        self.pageController.currentPageIndicatorTintColor = UIColor.green
        
        //self.view.addSubview(pageController)
        
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(self.pageController.currentPage) * scorllView.frame.size.width
        self.scorllView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.pageController.currentPage = Int(pageNumber)
    }
    
    
    
    

    
    
    
       //end of class
}
