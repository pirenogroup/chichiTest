//
//  Image.swift
//  chichiTest
//
//  Created by Arash on 12/16/1395 AP.
//  Copyright © 1395 arashgood. All rights reserved.
//

import Foundation


class Image {
    
   
    private var _imgTitle: String!
    private var _imgDesc: String!
    private var _imgStrUrl : String!
    
    
    var imgTitle: String {
        return _imgTitle
    }
    
    var  imgDesc: String {
        return _imgDesc
    }
    
    var imgStrUrl: String {
        return _imgStrUrl
    }
    
    init() {
        
    }
    
    
    init(title: String,desc: String ,StrUrl: String) {
        //self._imgId = id
        self._imgTitle = title
        self._imgDesc = desc
        self._imgStrUrl = StrUrl
    }
    
    
    
    
}

