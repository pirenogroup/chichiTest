//
//  Constants.swift
//  chichiTest
//
//  Created by Arash on 12/17/1395 AP.
//  Copyright © 1395 arashgood. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


let MAIN_URL = "https://dev.maghazam.com/api/v1/home"
let IMAGE_BASE_URL = ""


let imgTestURL = "https://en.opensuse.org/images/4/49/Amarok-logo-small.png"

let cellID = "TESTCELLL"

func get_img_from_strUrl(String strURL : String) -> UIImage {
    
    if !strURL.isEmpty {
    
    let imageURL = URL(string: strURL)!

        if let imageData = try NSData(contentsOf: imageURL)! as? Data {
            
            let image = UIImage(data: imageData)
            
            return image!
        } else {
            
           print("********** \n one image couldnt be downloaded **********")
           return UIImage(named: "placeholder")!
        }
    
    
    } else {
        print("********** \n one image couldnt be downloaded **********")
        return UIImage(named: "placeholder")!
    }

}

func showAllert(title: String, 	msg: String, vc: UIViewController){
    
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let action = UIAlertAction(title: "تایید", style: .default, handler: nil)
    alert.addAction(action)
    
    vc.present(alert, animated: true, completion: nil)
    
}

//internet checking **************************************************************************



//func isConnectedToNetwork() -> Bool {
//    
//    let url = URL(string: MAIN_URL)
//    
//    let data = NSData(contentsOf: url!) as Data?
//    
//    if data != nil {
//        return true
//    } else {
//        return false
//    }
//}


